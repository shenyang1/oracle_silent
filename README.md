# oracle_silent （静默安装oracle）
#### oracle 11g  通过自动应答文件 单机自动化安装shell脚本

# 版本信息
- oracle : 11.2.0.4 
- linux: centos 7


## 使用方法：
	* 以root身份运行 install-oracle11g.sh
	* 以oracle身份执行下面的命令，也就是上面脚本后面注释的部分

#### 解压数据库压缩包
```
mkdir -p /u01/oracle
unzip /home/database.zip -d /u01/oracle/
```


#### 解压数据库压缩包,预处理执行脚本

```
sh /home/oracle/install-oracle11g.sh
```

#### 切换oracle用户
```
su - oracle
source /home/oracle/.bash_profile
cd /u01/oracle/database
```

#### 安装数据库、创建数据库实例、创建网络监听服务
```
./runInstaller -silent -force -responseFile /home/oracle/db_install.rsp
dbca -silent -responseFile /home/oracle/dbca.rsp
netca -silent -responsefile /u01/oracle/database/response/netca.rsp
```

#### 配置开机启动
```
exit
cp /home/oracle/oracle.init.d /etc/init.d/oracle
chkconfig oracle on
```

## 安装过程可能报临时目录没有权限，执行下面命令
```
CVU_11.2.0.4.0_oracle chmod +x *.sh
CVU_11.2.0.4.0_oracle chown -R oracle:oinstall /tmp
```

## 默认设置：

ORACLE_SID=orcl

## 增强sqlplus的编辑功能

```
su -
cd /opt/oracle_install
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -ivh epel-release-latest-7.noarch.rpm
yum install readline rlwrap -y

su - oracle
echo 'alias sqlplus='"'"'rlwrap sqlplus'"'" >> /home/oracle/.bash_profile
echo 'alias rman='"'"'rlwrap rman'"'" >> /home/oracle/.bash_profile
```

## 美化sqlplus的显示


```
echo 'set linesize 300' >> $ORACLE_HOME/sqlplus/admin/glogin.sql
echo 'set pagesize 50000' >> $ORACLE_HOME/sqlplus/admin/glogin.sql
echo 'set timing on' >> $ORACLE_HOME/sqlplus/admin/glogin.sql
```

## oracle11g 下载

[百度网盘 oracle-11.2.0.4 ]
( 链接：https://pan.baidu.com/s/1i4kZ8QlDWJtiMzPhLIvbwQ 提取码：8r3v)

## 安装成功示例
[安装成功示例](https://gitee.com/qwop/oracle_silent/issues/I428YX)

## 添加 oracle 备份脚本
```
crontab -u oracle -e

47  22 * * * /home/oracle/oracle_bak

```

## 命令使用方法

```
通过指定以下参数创建数据库:
-createDatabase
        -templateName <默认位置或完整模板路径中现有模板的名称>
        [-cloneTemplate]
        -gdbName <全局数据库名>
        [-sid <数据库系统标识符>]
        [-sysPassword <SYS 用户口令>]
        [-systemPassword <SYSTEM 用户口令>]
        [-emConfiguration <CENTRAL|LOCAL|ALL|NONE>
                -dbsnmpPassword <DBSNMP 用户口令>
                -sysmanPassword <SYSMAN 用户口令>
                [-hostUserName <EM 备份作业的主机用户名>
                 -hostUserPassword <EM 备份作业的主机用户口令>
                 -backupSchedule <使用 hh:mm 格式的每日备份计划>]
                [-centralAgent <Enterprise Manager 中央代理主目录>]]
        [-disableSecurityConfiguration <ALL|AUDIT|PASSWORD_PROFILE|NONE>
        [-datafileDestination <所有数据库文件的目标目录> |  -datafileNames <含有诸如控制文件, 表空间, 重做日志文件数据库对象以及按 name=value 格式与这些对象相对应的裸设备文件名映射的 spfile 的文本文件。>]
        [-redoLogFileSize <每个重做日志文件的大小 (MB)>]
        [-recoveryAreaDestination <所有恢复文件的目标目录>]
        [-datafileJarLocation  <数据文件 jar 的位置, 只用于克隆数据库的创建>]
        [-storageType < FS | ASM > 
                [-asmsnmpPassword     <用于 ASM 监视的 ASMSNMP 口令>]
                 -diskGroupName   <数据库区磁盘组名>
                 -recoveryGroupName       <恢复区磁盘组名>
        [-characterSet <数据库的字符集>]
        [-nationalCharacterSet  <数据库的国家字符集>]
        [-registerWithDirService <true | false> 
                -dirServiceUserName    <目录服务的用户名>
                -dirServicePassword    <目录服务的口令>
                -walletPassword    <数据库 Wallet 的口令>]
        [-listeners  <监听程序列表, 该列表用于配置具有如下对象的数据库>]
        [-variablesFile   <用于模板中成对变量和值的文件名>]]
        [-variables  <以逗号分隔的 name=value 对列表>]
        [-initParams <以逗号分隔的 name=value 对列表>]
        [-sampleSchema  <true | false> ]
        [-memoryPercentage <用于 Oracle 的物理内存百分比>]
        [-automaticMemoryManagement ]
        [-totalMemory <为 Oracle 分配的内存 (MB)>]
        [-databaseType <MULTIPURPOSE|DATA_WAREHOUSING|OLTP>]]
```

## 启用EM控制台

#### 删除早期DBCONSOLE创建的用户
```
sql>drop role MGMT_USER;  
sql>drop user MGMT_VIEW cascade;  
sql>drop user sysman cascade;
```
#### 删除早期DBCONSOLE创建的对象
```
sql>drop PUBLIC SYNONYM MGMT_TARGET_BLACKOUTS;  
sql>drop public synonym SETEMVIEWUSERCONTEXT;  
```
#### 重新创建DBCONSOLE
```
emca -config dbcontrol db -repos create   
```

#### 如果提示创建失败，则需要执行一次重建的命令
```
emca -config dbcontrol db -repos recreate
```
#### 访问地址, SYS 用户登录

http://IP:1158/em
 