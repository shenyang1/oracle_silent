#!/bin/bash
yum install -y  gcc make binutils setarch compat-db compat-gcc-34 compat-gcc-34-c++ compat-libstdc++-33 unixODBC unixODBC-devel libaio-devel sysstat compat-gcc-34 compat-gcc-34-c++ unzip libXext
#set /etc/hosts
#passwd oracle
mkdir -p /u01/oracle
mkdir -p /u01/oraInventory

groupdel oinstall
groupdel dba
groupadd -g 10000 oinstall
groupadd -g 10001 dba
useradd -u 10000 -g oinstall -G dba oracle


chown -R oracle:oinstall /u01/
chmod -R 755 /u01/

mkdir -p /home/oracle/oraInventory
chown -R oracle:oinstall /home/oracle/oraInventory


#cat > /home/oracle/.bash_profile << EOF
#export ORACLE_BASE=/u01/oracle
#export ORACLE_HOME=$ORACLE_BASE/product/11.2.0/dbhome_1
#export ORACLE_SID=orcl
#export PATH=$PATH:$HOME/bin:$ORACLE_HOME/bin:$ORACLE_HOME/bin
#export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib
#export ORACLE_BASE ORACLE_HOME ORACLE_SID LD_LIBRARY_PATH PATH
#export NLS_LANG=AMERICAN
#export NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'
#EOF


echo 'export ORACLE_BASE=/u01/oracle' > /home/oracle/.bash_profile
echo 'export ORACLE_HOME=$ORACLE_BASE/product/11.2.0/dbhome_1' >> /home/oracle/.bash_profile
echo 'export ORACLE_SID=orcl' >> /home/oracle/.bash_profile
echo 'export PATH=$PATH:$HOME/bin:$ORACLE_HOME/bin:$ORACLE_HOME/bin' >> /home/oracle/.bash_profile
echo 'export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib' >> /home/oracle/.bash_profile
echo 'export ORACLE_BASE ORACLE_HOME ORACLE_SID LD_LIBRARY_PATH PATH' >> /home/oracle/.bash_profile
#echo 'export NLS_LANG=AMERICAN' >> /home/oracle/.bash_profile
echo "export NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'" >> /home/oracle/.bash_profile

source /home/oracle/.bash_profile 

cat > /etc/security/limits.conf << EOF
oracle soft nproc 2047
oracle hard nproc 16384
oracle soft nofile 1024
oracle hard nofile 65536
oracle soft stack 10240
oracle hard stack 10240
EOF


cat > /etc/sysctl.conf << EOF
kernel.shmall = 2097152
#kernel.shmmax最低：536870912，最大值：比物理内存小1个字节的值，建议超过物理内存的一半
kernel.shmmax = 2147483648 
fs.file-max = 6815744
fs.aio-max-nr = 1048576
kernel.shmmni = 4096
kernel.sem = 250 32000 100 128
net.ipv4.ip_local_port_range = 9000 65500
net.core.rmem_default = 262144
net.core.rmem_max = 4194304
net.core.wmem_default = 262144
net.core.wmem_max = 1048576
EOF

sysctl -p


cat > /etc/oraInst.loc << EOF
UNIX_GROUP_NAME=oinstall
INVENTORY_LOCATION=/u01/oraInventory
EOF


#unzip linux.x64_11gR2_database_1of2.zip -d /u01/oracle/
#unzip linux.x64_11gR2_database_2of2.zip -d /u01/oracle/
chown -R oracle:dba /u01/oracle
sed -i '/CV_ASSUME_DISTID/s/4/6/g' /u01/oracle/database/stage/cvu/cv/admin/cvu_config
#su - oracle
#cd /u01/oracle/database
#./runInstaller -silent -force -responseFile /home/oracle/db_install.rsp
#dbca -silent -responseFile /home/oracle/dbca.rsp 
#netca -silent -responsefile /u01/oracle/database/response/netca.rsp
#cp oracle.init.d /etc/init.d/oracle
#chkconfig oracle on
